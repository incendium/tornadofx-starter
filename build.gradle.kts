group = "com.example"
version = "1.0-SNAPSHOT"

plugins {
    application
    kotlin("jvm") version "1.5.20"
    id("org.openjfx.javafxplugin") version "0.0.10"
}

repositories {
    mavenCentral()

    // TornadoFX snapshots for JFX 11+
    maven("https://oss.sonatype.org/content/repositories/snapshots") {
        name = "tornadofx-snapshots"
    }
}

application {
    mainClass.set("com.example.MainKt")
}

dependencies {
    implementation("no.tornado:tornadofx:2.0.0-SNAPSHOT")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "15"
}

javafx {
    version = "16"
    modules = listOf("javafx.controls", "javafx.fxml", "javafx.media", "javafx.swing", "javafx.web")
}
